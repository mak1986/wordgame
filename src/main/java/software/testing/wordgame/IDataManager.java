package software.testing.wordgame;

import java.util.List;
import java.util.Map;

public interface IDataManager {
	/**
	 * 
	 * @return map ที่เก็บข้อมูลว่า ตัวอักขระใดได้คะแนนเท่าใด
	 */
	Map<Character, Integer> getCharacterScoreMap();
	/**
	 * 
	 * @return list ที่เก็บข้อมูลคำ
	 */
	List<String> getWordList();
}
