package software.testing.wordgame;

import java.util.Map;

public class LetterScoreList {

	private IDataManager manager;
	private Map<Character, Integer> scoreMap;

	/**
	 * Constructor ที่สร้าง LetterScoreList ซึ่งเก็บข้อมูลว่า ตัวอักขระใด ได้คะแนนเท่าใด
	 * @param mgr เป็นตัวดึงข้อมูลตัวอักขระและคะแนนมาให้ LetterScoreList
	 */
	public LetterScoreList(IDataManager mgr) {
		this.manager = mgr;
		this.scoreMap = this.manager.getCharacterScoreMap();
	}

	/**
	 * ให้ผลรวมคะแนนของคำ เช่น ถ้า a = 1, c = 2, t = 1 และคำเป็น cat จะต้อง return 4 
	 * @param word
	 * @return ผลรวมคะแนนของอักขระในคำนั้นๆ (ไม่รวม 50 คะแนนพิเศษ)
	 */
	public int getWordScores(String word){
		if(word == null || word.equals("")){
			throw new IllegalArgumentException("word must not be null or an empty string");
		}
		String lcWord = word.toLowerCase();		
		int scores = 0;
		for (char ch: lcWord.toCharArray()) {
			if(!this.scoreMap.containsKey(ch)){
				throw new IllegalArgumentException("Character does not exists in score map");
			}
			scores += scoreMap.get(ch);
		}
		return scores;
	}
}
