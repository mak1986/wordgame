package software.testing.wordgame;

public class WordGame {
	private int handSize;
	private WordList wordList;
	private LetterScoreList letterScoreList;
	private Player player;
	
	/**
	 * Constructor ที่สร้าง WordGame
	 * @param handSize เป็นขนาดของตัวอักษรในมือ
	 * @param wordList เป็นตัวเก็บคำ
	 * @param scores เป็นตัวเก็บข้อมูลตัวอักขระและคะแนน
	 * @param player เป็นผู้เล่น
	 */
	public WordGame(int handSize, WordList wordList, LetterScoreList scores, Player player){
		this.handSize = handSize;
		this.wordList = wordList;
		this.letterScoreList = scores;
		this.player = player;
		
	}
	
	/**
	 * 1. ตรวจสอบว่า คำที่ผู้เล่นส่งมา เป็นคำที่อยู่ใน WordList หรือไม่ และใช้ตัวอักษรที่มีอยู่ในมือได้หรือไม่
	 * 2. ถ้าได้ทั้งคู่ ก็เพิ่มคะแนนให้ผู้เล่นตามคะแนนของคำ และปรับจำนวนตัวอักษรในมือผู้เล่นตามคำด้วย
	 * 3. ถ้าคำมีขนาดเท่ากับ n (ตัวอักษรในมือที่ให้แต่แรก เช่น 7) จะเพิ่มคะแนนให้ผู้เล่นอีก 50 คะแนน
	 * @param word คำที่ผู้เล่นส่งมา
	 * @return คะแนนของคำที่ผู้เล่นได้ ( ได้ 0 คะแนนถ้าคำไม่ถูกต้อง)
	 */
	public int tryWord(String word){
		int points = 0;
		boolean boolWordListContainsWord = this.wordList.containsWord(word);
		boolean boolHandContainsLetter = this.player.getHand().containsLetters(word);
		if(boolHandContainsLetter && boolWordListContainsWord){
			points = this.letterScoreList.getWordScores(word);
			if(this.handSize == word.length()){
				points += 50;
			}
			this.player.addPoints(points);
			this.player.getHand().update(word);
		}
		return points;
	}
	/**
	 * 
	 * @return คะแนนรวมของผู้เล่นในขณะนั้น
	 */
	public int getPlayerPoints(){
		return this.player.getPoints();
	}
	/**
	 * 
	 * @return Hand ของผู้เล่นในขณะนั้น
	 */
	public Hand getPlayerHand(){
		return this.player.getHand();
	}
}
