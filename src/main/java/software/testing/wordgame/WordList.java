package software.testing.wordgame;

import java.util.List;

public class WordList {

	private IDataManager manager;
	private List<String> list;
	
	/**
	 * Constructor ที่สร้าง WordList ซึ่งเก็บข้อมูลคำที่เล่นได้ทั้งหมด
	 * @param mgr เป็นตัวดึงข้อมูลคำมาให้ WordList
	 */
	public WordList(IDataManager mgr){
		manager = mgr;
		list = this.manager.getWordList();
	}
	/**
	 * 
	 * @param word
	 * @return true เมื่อคำที่ให้อยู่ใน List และ return false ถ้าไม่อยู่ใน list
	 */
	public boolean containsWord(String word){
		return list.contains(word);
	}
}
