package software.testing.wordgame;

import java.util.HashMap;
import java.util.Map;

public class Hand {
	private Map<Character, Integer> handFreq = new HashMap<Character, Integer>();
	public Hand() {}
	/**
	 * เพิ่มตัวอักษรในมือผู้เล่น
	 * @param letter เป็นตัวอักษรที่ต้องการเพิ่ม
	 */
	public void addLetter(char letter){
		if(this.handFreq.containsKey(letter)){
			this.handFreq.put(letter, this.handFreq.get(letter)+1);
		}else{
			this.handFreq.put(letter, 1);
		}
	}
	/**
	 * ตรวจสอบว่า ตัวอักษรในมือผู้เล่นสามารถนำไปผสมเป็นคำ word ได้หรือไม่
	 * @param word คำที่ผู้เล่นเลือก
	 * @return true ถ้าตัวอักษรในมือผู้เล่นสามารถนำไปผสมเป็นคำ word ได้และ false ถ้าไม่ได้
	 * @throws IllegalAccessException ถ้า word เป็น null หรือ ""
	 */
	public boolean containsLetters(String word) {
		if(word == null || word.equals("")){
			throw new IllegalArgumentException();
		}
		HashMap<Character, Integer> temp = new HashMap<Character, Integer>(this.handFreq);
		
		for (int i = 0; i < word.length(); i++) {
			if (temp.containsKey(word.charAt(i)) && temp.get(word.charAt(i))>0) {
				temp.put(word.charAt(i), temp.get(word.charAt(i))-1);
			}else{
				return false;
			}
		}
		return true;
	}
	/**
	 * ปรับตัวอักษรที่อยู่ในมือผู้เล่น โดยนำตัวอักษรของผู้เล่นที่ตรงกับตัวอักษรใน word ออกทุกตัว
	 * @param word คำที่ผู้เล่นเลือก
	 * @throws IllegalArgumentException ถ้าตัวอักษรในมือผสมเป็น word ไม่ได้
	 */
	public void update(String word) {
		if(!this.containsLetters(word)){
			throw new IllegalArgumentException();
		}
		for (int i = 0; i < word.length(); i++) {
			Character character = word.charAt(i);
			int characterCount = this.handFreq.get(character);
			if (this.handFreq.containsKey(character) && characterCount>0) {
				if(characterCount-1>0){
					this.handFreq.put(character, characterCount-1);
				} else {
					this.handFreq.remove(character);
				}
			}
		}
	}
	public boolean isEmpty() {
		return this.handFreq.isEmpty();
	}
	protected Map<Character, Integer> getHandFreq(){
		return this.handFreq;
	}
	@Override
	public String toString(){

		String result = "\n";
		for(Map.Entry<Character, Integer> keyValue:this.handFreq.entrySet() ){
			result += keyValue.getKey()+" : " + keyValue.getValue() + "\n";
		}
		return result;

	}
}
