package software.testing.wordgame;

public class Player {
	private Hand hand;
	private int points;
	/**
	 * Constructor ที่สร้างผู้เล่นด้วยอักษรเริ่มต้นที่ให้
	 * @param hand เป็นตัวอักษรเริ่มต้นของผู้เล่นคนนี้
	 */
	public Player(Hand hand) {
		this.hand = hand;
	}

	/**
	 * 
	 * @return object Hand ของผู้เล่นคนนี้
	 */
	public Hand getHand() {
		return this.hand;
	}

	/**
	 * เพิ่มคะแนนให้กับผู้เล่นคนนี้
	 * @param points คะแนนที่ต้องการเพิ่ม
	 */
	public void addPoints(int points) {
		this.points += points;
	}

	/**
	 * 
	 * @return คะแนนรวมผู้เล่นคนนี้
	 */
	public int getPoints() {
		return this.points;
	}

	public boolean equals(Object object) {
		boolean result = false;
		if(object instanceof Player){
			result = (((Player) object).getPoints() == this.points);
		}
		return result;
	}
}
