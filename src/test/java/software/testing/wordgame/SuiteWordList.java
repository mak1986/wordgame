package software.testing.wordgame;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestWordList.class, TestWordListWithJMock.class })
public class SuiteWordList {

}
