package software.testing.wordgame;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestLetterScoreList.class, TestLetterScoreListException.class })
public class SuiteLetterScoreList {

}
