package software.testing.wordgame;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class TestWordList {

	private WordList list;

	@Before
	public void setUp() throws Exception {
		list = new WordList(new StubDataManager());
	}

	@Test
	public void testContrainsWordTrue() {
		boolean result = this.list.containsWord("cat");
		assertTrue(result);
	}
	@Test
	public void testContrainsWordFalse() {
		boolean result = this.list.containsWord("dog");
		assertFalse(result);
	}
	
	private class StubDataManager implements IDataManager{

		@Override
		public Map<Character, Integer> getCharacterScoreMap() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<String> getWordList() {
			ArrayList<String> list = new ArrayList<String>();
			list.add("cat");
			return list;
		}
		
	}

}
