package software.testing.wordgame;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class TestHandQualityEfficiency {

	Hand handFreqMap;

	@Before
	public void setup() {
		this.handFreqMap = new Hand();
		this.handFreqMap.addLetter('a');
	}
	
	@Test(timeout = 10)
	public void testContainsLetters() {
		boolean result = false;
		for (int i = 0; i < 9999; i++) {
			result = this.handFreqMap.containsLetters("a");
		}
		assertTrue(result);
	}

}
