package software.testing.wordgame;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class TestLetterScoreList {

	StubDataManager manager;
	LetterScoreList letterScoreList;
	
	@Before
	public void setUp() throws Exception {
		this.manager = new StubDataManager();
		this.letterScoreList = new LetterScoreList(this.manager);
	}

	@Test
	public void testGetWordScoresLowerCase() {
		int scores = this.letterScoreList.getWordScores("cat");
		assertEquals(4,scores);
	}
	@Test
	public void testGetWordScoresUpperCase() {
		int scores = this.letterScoreList.getWordScores("CAT");
		assertEquals(4,scores);
	}
	@Test
	public void testGetWordScoresLowerCaseAndUpperCase() {
		int scores = this.letterScoreList.getWordScores("Cat");
		assertEquals(4,scores);
	}

	private class StubDataManager implements IDataManager{

		@Override
		public Map<Character, Integer> getCharacterScoreMap() {
			Map<Character, Integer> map = new HashMap<Character, Integer>();
			map.put('c', 2);
			map.put('a', 1);
			map.put('t', 1);
			return map;
		}

		@Override
		public List<String> getWordList() {
			return null;
		}
		
	}
}
