package software.testing.wordgame;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class TestHandException {
	Hand handFreqMap;

	@Before
	public void setup() {
		this.handFreqMap = new Hand();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testContainsLettersThrowsExceptionNullName() {
		this.handFreqMap.containsLetters(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testContainsLettersThrowsExceptionEmptyName() {
		this.handFreqMap.containsLetters("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUpdateThrowsExceptionImpossibleUpdate() {
		this.handFreqMap.update("123456789abcdefghijklmnopqrstuvwxyz");
	}
}
