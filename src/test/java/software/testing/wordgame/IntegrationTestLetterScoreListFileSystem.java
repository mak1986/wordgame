package software.testing.wordgame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class IntegrationTestLetterScoreListFileSystem {

	FileDataManager manager;
	LetterScoreList letterScoreList;
	
	@Before
	public void setUp() throws Exception {
		this.manager = new FileDataManager();
		this.letterScoreList = new LetterScoreList(this.manager);
	}

	@Test
	public void testGetWordScoresLowerCase() {
		int scores = this.letterScoreList.getWordScores("cat");
		assertEquals(6,scores);
	}
	@Test
	public void testGetWordScoresUpperCase() {
		int scores = this.letterScoreList.getWordScores("CAT");
		assertEquals(6,scores);
	}
	@Test
	public void testGetWordScoresLowerCaseAndUpperCase() {
		int scores = this.letterScoreList.getWordScores("Cat");
		assertEquals(6,scores);
	}

}
