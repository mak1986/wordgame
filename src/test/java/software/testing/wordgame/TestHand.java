package software.testing.wordgame;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class TestHand {
	Hand handBasic;
	Hand handIntermediate;

	@Before
	public void setup() {
		this.setupHandBasic();
		this.setupHandIntermediate();
	}

	private void setupHandBasic() {
		this.handBasic = new Hand();
		this.handBasic.addLetter('r');
		this.handBasic.addLetter('a');
		this.handBasic.addLetter('a');
		this.handBasic.addLetter('t');
		this.handBasic.addLetter('t');
		this.handBasic.addLetter('t');
		this.handBasic.addLetter('c');
	}

	private void setupHandIntermediate() {
		this.handIntermediate = new Hand();
		this.handIntermediate.addLetter('a');
		this.handIntermediate.addLetter('a');
		this.handIntermediate.addLetter('b');
		this.handIntermediate.addLetter('b');
		this.handIntermediate.addLetter('c');
		this.handIntermediate.addLetter('c');
	}

	@Test
	public void testContainsLettersBasic() {
		assertTrue(this.handBasic.containsLetters("cat"));
		assertTrue(this.handBasic.containsLetters("raatttc"));
		assertFalse(this.handBasic.containsLetters("abc"));
	}

	@Test
	public void testUpdateBasic() {
		this.handBasic.update("rac");
		assertFalse(this.handBasic.isEmpty());
		this.handBasic.update("attt");
		assertTrue(this.handBasic.isEmpty());
	}

	@Test
	public void testIsEmptyBasic() {
		this.handBasic.update("raatttc");
		assertTrue(this.handBasic.isEmpty());
	}

	@Test
	public void testUpdateIntermediate1() {
		this.handIntermediate.update("ab");
		assertTrue(this.handIntermediate.containsLetters("abcc"));
		assertFalse(this.handIntermediate.containsLetters("aa"));
		assertFalse(this.handIntermediate.containsLetters("bb"));
	}

	/**
	 * การทดสอบค่าภายในแบบนี้ ข้อดีคือทำให้มั่นใจว่า modifier ภายใน class
	 * ทำงานถูกต้อง ข้อเสียคือ ต้อสร้าง get method เพิ่ม
	 */
	@Test
	public void testUpdateIntermediate2() {
		this.handIntermediate.update("ab");
		Map<Character, Integer> handFreq = this.handIntermediate.getHandFreq();
		assertEquals(3, handFreq.size());
		assertEquals(1, (int) handFreq.get('a'));
		assertEquals(1, (int) handFreq.get('b'));
		assertEquals(2, (int) handFreq.get('c'));
	}
}
