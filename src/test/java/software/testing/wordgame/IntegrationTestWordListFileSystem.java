package software.testing.wordgame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class IntegrationTestWordListFileSystem {

	private WordList list;

	@Before
	public void setUp() throws Exception {
		list = new WordList(new FileDataManager());
	}

	@Test
	public void testContrainsWordTrue() {
		boolean result = this.list.containsWord("cat");
		assertTrue(result);
	}
	@Test
	public void testContrainsWordFalse() {
		boolean result = this.list.containsWord("asdfasdfasdf");
		assertFalse(result);
	}

}
