package software.testing.wordgame;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class IntegrationTestHandPlayer {

	private Player player;
	private Hand hand;
	
	@Before
	public void setUp() throws Exception {
		this.hand = new Hand();
		this.hand.addLetter('a');
		this.player = new Player(hand);
	}

	@Test
	public void testGetHandSame() {
		Hand playersHand = this.player.getHand();
		assertSame(playersHand,this.hand);
	}
	@Test
	public void testGetHandEqual() {
		Hand playersHand = this.player.getHand();
		assertEquals(playersHand,this.hand);
	}
}
