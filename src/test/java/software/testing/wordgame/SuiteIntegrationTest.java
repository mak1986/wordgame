package software.testing.wordgame;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ IntegrationTestHandPlayer.class,
		IntegrationTestLetterScoreListFileSystem.class,
		IntegrationTestWordGame.class, IntegrationTestWordGamePlayerHand.class,
		IntegrationTestWordGamePlayerHandWordListFileDataManager.class,
		IntegrationTestWordListFileSystem.class })
public class SuiteIntegrationTest {

}
