package software.testing.wordgame;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class TestPlayer {

	private Player player;
	private Hand hand;
	
	@Before
	public void setUp() throws Exception {
		this.hand = new StubHand();
		this.hand.addLetter('a');
		this.player = new Player(hand);
	}

	@Test
	public void testGetHandSame() {
		Hand playersHand = this.player.getHand();
		assertSame(playersHand,this.hand);
	}
	@Test
	public void testGetHandEqual(){
		Hand playersHand = this.player.getHand();
		assertEquals(playersHand,this.hand);
	}
	@Test
	public void TestAddPointOnce(){
		this.player.addPoints(1);
		assertEquals(1, this.player.getPoints());
	}
	@Test
	public void testAddPointTwice(){
		this.player.addPoints(2);
		this.player.addPoints(3);
		assertEquals(5, this.player.getPoints());
	}

	private class StubHand extends Hand{
		
		public boolean containsLetters(String word) {
			return true;
		}
		public void update(String word) {

		}
		public boolean isEmpty() {
			return true;
		}
		protected Map<Character, Integer> getHandFreq(){
			return null;
			
		}
	}
}
