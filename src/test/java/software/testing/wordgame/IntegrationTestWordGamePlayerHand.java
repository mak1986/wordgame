package software.testing.wordgame;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class IntegrationTestWordGamePlayerHand {

	private WordGame wordGame;
	private StubWordList stubWordList;
	private StubLetterScoreList stubLetterScoreList;
	private Player player;
	private Hand hand;
	
	
	@Before
	public void setUp() throws Exception {
		this.stubWordList = new StubWordList(new StubIDataManager());
		this.stubLetterScoreList = new StubLetterScoreList(new StubIDataManager());
		this.hand = new Hand();
		this.hand.addLetter('w');
		this.hand.addLetter('o');
		this.hand.addLetter('m');
		this.hand.addLetter('a');
		this.hand.addLetter('n');
		this.player = new Player(this.hand);
		this.wordGame = new WordGame(5, this.stubWordList, this.stubLetterScoreList, this.player);
	}

	@Test
	public void TestTryWordExistInWordListEqualHandSize() {
		int points = this.wordGame.tryWord("woman");
		assertEquals(60,points);
	}
	@Test
	public void TestTryWordExistInWordListLessThanHandSize() {
		int points = this.wordGame.tryWord("man");
		assertEquals(5,points);
	}
	@Test
	public void TestTryWordDoesNotExistInWordList() {
		int points = this.wordGame.tryWord("dogs");
		assertEquals(0,points);
	}
	@Test
	public void TestGetPlayerPointsZero(){
		int points = this.wordGame.getPlayerPoints();
		assertEquals(0, points);
	}
	@Test
	public void TestGetPlayerPointsAfterTryWordEqualHandSize(){
		this.wordGame.tryWord("woman");
		assertEquals(60,this.wordGame.getPlayerPoints());
	}
	@Test
	public void TestGetPlayerPointsAfterTryWordLessThanHandSize(){
		this.wordGame.tryWord("man");
		assertEquals(5,this.wordGame.getPlayerPoints());
	}
	@Test
	public void TestGetHandSame(){
		Hand hand = this.wordGame.getPlayerHand();
		assertSame(this.hand,hand);
	}
	
	private class StubWordList extends WordList{

		public StubWordList(IDataManager mgr) {
			super(mgr);
		}
		
	}
	private class StubLetterScoreList extends LetterScoreList{
		public StubLetterScoreList(IDataManager mgr) {
			super(mgr);
		}
		
	}
	
	private class StubIDataManager implements IDataManager{

		@Override
		public Map<Character, Integer> getCharacterScoreMap() {
			Map<Character, Integer> map = new HashMap<Character, Integer>();
			map.put('w', 4);
			map.put('o', 1);
			map.put('m', 2);
			map.put('a', 1);
			map.put('n', 2);
			return map;
		}

		@Override
		public List<String> getWordList() {
			ArrayList<String> list = new ArrayList<String>();
			list.add("woman");
			list.add("man");
			return list;
		}
		
	}
}
