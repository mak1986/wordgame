package software.testing.wordgame;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class TestWordGame {

	private WordGame wordGame;
	private StubWordList stubWordList;
	private StubLetterScoreList stubLetterScoreList;
	private StubPlayer stubPlayer;
	private StubHand stubHand;
	
	
	@Before
	public void setUp() throws Exception {
		this.stubWordList = new StubWordList(new StubIDataManager());
		this.stubLetterScoreList = new StubLetterScoreList(new StubIDataManager());
		this.stubHand = new StubHand();
		this.stubHand.addLetter('c');
		this.stubHand.addLetter('a');
		this.stubHand.addLetter('t');
		this.stubHand.addLetter('s');
		this.stubPlayer = new StubPlayer(this.stubHand);
		this.wordGame = new WordGame(4, this.stubWordList, this.stubLetterScoreList, this.stubPlayer);
	}

	@Test
	public void TestTryWordExistInWordListEqualHandSize() {
		int points = this.wordGame.tryWord("cats");
		assertEquals(55,points);
	}
	@Test
	public void TestTryWordExistInWordListLessThanHandSize() {
		int points = this.wordGame.tryWord("cat");
		assertEquals(4,points);
	}
	@Test
	public void TestTryWordDoesNotExistInWordList() {
		int points = this.wordGame.tryWord("dog");
		assertEquals(0,points);
	}
	@Test
	public void TestGetPlayerPointsZero(){
		int points = this.wordGame.getPlayerPoints();
		assertEquals(0, points);
	}
	@Test
	public void TestGetPlayerPointsAfterTryWordEqualHandSize(){
		this.wordGame.tryWord("cats");
		assertEquals(55,this.wordGame.getPlayerPoints());
	}
	@Test
	public void TestGetPlayerPointsAfterTryWordLessThanHandSize(){
		this.wordGame.tryWord("cat");
		assertEquals(4,this.wordGame.getPlayerPoints());
	}
	@Test
	public void TestGetHandSame(){
		Hand hand = this.wordGame.getPlayerHand();
		assertSame(this.stubHand,hand);
	}
	private class StubPlayer extends Player{

		public StubPlayer(Hand hand) {
			super(hand);
		}
		
	}
	private class StubWordList extends WordList{

		public StubWordList(IDataManager mgr) {
			super(mgr);
		}
		
	}
	private class StubLetterScoreList extends LetterScoreList{
		public StubLetterScoreList(IDataManager mgr) {
			super(mgr);
		}
		
	}
	private class StubHand extends Hand{}
	private class StubIDataManager implements IDataManager{

		@Override
		public Map<Character, Integer> getCharacterScoreMap() {
			Map<Character, Integer> map = new HashMap<Character, Integer>();
			map.put('c', 2);
			map.put('a', 1);
			map.put('t', 1);
			map.put('s', 1);
			return map;
		}

		@Override
		public List<String> getWordList() {
			ArrayList<String> list = new ArrayList<String>();
			list.add("cat");
			list.add("cats");
			return list;
		}
		
	}
}
