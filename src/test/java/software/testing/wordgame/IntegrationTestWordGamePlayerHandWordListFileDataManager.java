package software.testing.wordgame;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class IntegrationTestWordGamePlayerHandWordListFileDataManager {

	private WordGame wordGame;
	private WordList wordList;
	private StubLetterScoreList stubLetterScoreList;
	private Player player;
	private Hand hand;
	
	
	@Before
	public void setUp() throws Exception {
		this.wordList = new WordList(new FileDataManager());
		this.stubLetterScoreList = new StubLetterScoreList(new FileDataManager());
		this.hand = new Hand();
		this.hand.addLetter('w');
		this.hand.addLetter('o');
		this.hand.addLetter('m');
		this.hand.addLetter('a');
		this.hand.addLetter('n');
		this.player = new Player(this.hand);
		this.wordGame = new WordGame(5, this.wordList, this.stubLetterScoreList, this.player);
	}

	@Test
	public void TestTryWordExistInWordListEqualHandSize() {
		int points = this.wordGame.tryWord("woman");
		assertEquals(60,points);
	}
	@Test
	public void TestTryWordExistInWordListLessThanHandSize() {
		int points = this.wordGame.tryWord("man");
		System.out.print(points);
		assertEquals(5,points);
	}
	@Test
	public void TestTryWordDoesNotExistInWordList() {
		int points = this.wordGame.tryWord("dogs");
		assertEquals(0,points);
	}
	@Test
	public void TestGetPlayerPointsZero(){
		int points = this.wordGame.getPlayerPoints();
		assertEquals(0, points);
	}
	@Test
	public void TestGetPlayerPointsAfterTryWordEqualHandSize(){
		this.wordGame.tryWord("woman");
		assertEquals(60,this.wordGame.getPlayerPoints());
	}
	@Test
	public void TestGetPlayerPointsAfterTryWordLessThanHandSize(){
		this.wordGame.tryWord("man");
		assertEquals(5,this.wordGame.getPlayerPoints());
	}

	
	private class StubLetterScoreList extends LetterScoreList{
		public StubLetterScoreList(IDataManager mgr) {
			super(mgr);
		}
		
	}

}
