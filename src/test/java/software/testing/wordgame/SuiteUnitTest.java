package software.testing.wordgame;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SuiteHandTests.class, SuiteLetterScoreList.class,
		SuiteWordGame.class, SuiteWordList.class, TestPlayer.class })
public class SuiteUnitTest {

}
