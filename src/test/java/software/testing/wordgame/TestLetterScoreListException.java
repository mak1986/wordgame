package software.testing.wordgame;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class TestLetterScoreListException {

	StubDataManager manager;
	LetterScoreList letterScoreList;

	@Before
	public void setup() {
		this.manager = new StubDataManager();
		this.letterScoreList = new LetterScoreList(this.manager);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testContainsLettersThrowsExceptionCharacterDoesNotExists() {
		this.letterScoreList.getWordScores("dog");
	}
	@Test(expected = IllegalArgumentException.class)
	public void testContainsLettersThrowsExceptionNotNull() {
		this.letterScoreList.getWordScores(null);
	}
	@Test(expected = IllegalArgumentException.class)
	public void testContainsLettersThrowsExceptionNotEmpty() {
		this.letterScoreList.getWordScores("");
	}
	private class StubDataManager implements IDataManager{

		@Override
		public Map<Character, Integer> getCharacterScoreMap() {
			Map<Character, Integer> map = new HashMap<Character, Integer>();
			map.put('c', 2);
			map.put('a', 1);
			map.put('t', 2);
			return map;
		}

		@Override
		public List<String> getWordList() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
}
