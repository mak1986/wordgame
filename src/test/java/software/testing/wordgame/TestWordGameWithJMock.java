package software.testing.wordgame;

import static org.junit.Assert.*;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMock.class)
public class TestWordGameWithJMock {

	private Mockery context = new JUnit4Mockery(){{
		setImposteriser(ClassImposteriser.INSTANCE);
	}};
	
	@Test
	public void testWordGameTryWordValidUpdateAddPoint(){
		final WordList stubWordList = this.context.mock(WordList.class);
		final LetterScoreList stubLetterScoreList = this.context.mock(LetterScoreList.class);
		final Player mockPlayer = this.context.mock(Player.class);
		final Hand mockHand = this.context.mock(Hand.class);
		
		final String word = "cat";

		this.context.checking(new Expectations(){{
			//setup mockHand
			allowing(mockPlayer).getHand();				
				will(returnValue(mockHand));
			allowing(stubWordList).containsWord(word);
				will(returnValue(true));
			allowing(mockHand).containsLetters(word);
				will(returnValue(true));
			allowing(stubLetterScoreList).getWordScores(word);
				will(returnValue(5));
			
			oneOf(mockPlayer).addPoints(55);
			oneOf(mockHand).update(word);
		}});
		WordGame wordGame = new WordGame(3, stubWordList, stubLetterScoreList, mockPlayer);
		int score = wordGame.tryWord("cat");
		assertEquals(55, score);
	}

	@Test
	public void testWordGameTryWordNeverUpdate(){
		final WordList stubWordList = this.context.mock(WordList.class);
		final LetterScoreList stubLetterScoreList = this.context.mock(LetterScoreList.class);
		final Player stubPlayer = this.context.mock(Player.class);
		final Hand mockHand = this.context.mock(Hand.class);
		
		this.context.checking(new Expectations(){{
			allowing(stubPlayer).getHand();
				will(returnValue(mockHand));
			allowing(stubWordList).containsWord(with(any(String.class)));
				will(returnValue(false));
			allowing(mockHand).containsLetters(with(any(String.class)));
				will(returnValue(false));
			
			never(mockHand).update(with(any(String.class)));			
		}});
		WordGame wordGame = new WordGame(4, stubWordList, stubLetterScoreList, stubPlayer);
		assertEquals(0,wordGame.tryWord("cat"));
	}
	
	@Test
	public void testWordGameTryWordSequence(){
		final Sequence sequence = context.sequence("sequence");
		final LetterScoreList mockLetterScoreList = this.context.mock(LetterScoreList.class);
		final Player mockPlayer = this.context.mock(Player.class);
		final Hand stubHand = this.context.mock(Hand.class);
		final WordList stubWordList = this.context.mock(WordList.class);
		
		this.context.checking(new Expectations(){{
			allowing(mockPlayer).getHand();
				will(returnValue(stubHand));
			allowing(stubHand).update(with(any(String.class)));
			allowing(stubHand).containsLetters(with(any(String.class)));
				will(returnValue(true));
			allowing(stubWordList).containsWord(with(any(String.class)));
				will(returnValue(true));
			
			oneOf(mockLetterScoreList).getWordScores(with(any(String.class)));
				inSequence(sequence);
			oneOf(mockPlayer).addPoints(with(any(Integer.class)));
				inSequence(sequence);
				
		}});
		
		WordGame wordGame = new WordGame(4, stubWordList, mockLetterScoreList, mockPlayer);
		assertEquals(0,wordGame.tryWord("cat"));
	}

}
